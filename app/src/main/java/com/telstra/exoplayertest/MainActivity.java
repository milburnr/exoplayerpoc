package com.telstra.exoplayertest;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.telstra.exoplayertest.fragments.MainFragment;

public class MainActivity extends AppCompatActivity {

	public static final int INTERNET_PERMISSION_REQUEST_CODE = 1;
	public static final String TAG = MainActivity.class.toString();
	private String streamUri = "http://irtdashreference-i.akamaihd.net/dash/live/901161/bfs/manifestARD.mpd";

	private MainFragment retainedFragment;
	private boolean destroyedBySystem;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
		setContentView(R.layout.activity_main);
		checkPermission();

		retainedFragment = findOrCreate(getSupportFragmentManager(), TAG);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.i(TAG, "onSaveInstanceState");
		destroyedBySystem = true;
	}

	public MainFragment findOrCreate(FragmentManager fm, String tag)
	{
		MainFragment retainedFragment = (MainFragment) fm.findFragmentByTag(tag);

		if (retainedFragment == null)
		{
			retainedFragment = new MainFragment();
			fm.beginTransaction()
				.add(R.id.videoFragmentContainer, retainedFragment, tag)
				.commitAllowingStateLoss();
		}

		return  retainedFragment;
	}

	public void remove(FragmentManager fm)
	{
		if (retainedFragment != null)
		{
			if (!fm.isDestroyed()){
				fm.beginTransaction()
						.remove(retainedFragment)
						.commitAllowingStateLoss();
				retainedFragment.releasePlayer();
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i(TAG, "onStart");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");
		destroyedBySystem = false;
		if (retainedFragment != null)
		{
			retainedFragment.setPlayWhenReady(true);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i(TAG, "onPause");
		if (retainedFragment != null)
		{
			retainedFragment.setPlayWhenReady(false);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.i(TAG, "onStop");
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy");
		if (destroyedBySystem)
		{
			onDestroyBySystem();
		}
		else{
			onDestroyByUser();
		}

		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onDestroy();
	}

	public void onDestroyByUser() {
		Log.i(TAG, "onDestroyByUser");
		remove(getSupportFragmentManager());
		retainedFragment.releasePlayer();
		retainedFragment.setData(null);
		retainedFragment = null;
	}

	public void onDestroyBySystem() {
		Log.i(TAG, "onDestroyBySystem");
	}


	private void checkPermission()
	{
		int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
		if (permissionCheck != PackageManager.PERMISSION_GRANTED){
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, INTERNET_PERMISSION_REQUEST_CODE);
			return;
		}
	}


	public MainFragment getRetainedFragment() {
		return retainedFragment;
	}

	public void setRetainedFragment(MainFragment retainedFragment) {
		this.retainedFragment = retainedFragment;
	}
}

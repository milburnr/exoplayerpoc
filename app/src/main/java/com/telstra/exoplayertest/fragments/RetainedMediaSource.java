package com.telstra.exoplayertest.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.telstra.exoplayertest.utils.EventLogger;

/**
 * Created by russellmilburn on 19/1/17.
 */

public class RetainedMediaSource {

	private Handler mainHandler;
	private DefaultDataSourceFactory defaultDataSourceFactory;
	private DefaultDashChunkSource.Factory chunkFactory;
	private DefaultHttpDataSourceFactory httpDataSourceFactory;
	private EventLogger eventLogger;
	private MediaSource mediaSource;
	private SimpleExoPlayer player;


	public RetainedMediaSource(Context context, Uri uri)
	{
		mainHandler = new Handler();
		eventLogger = new EventLogger();
		httpDataSourceFactory = new DefaultHttpDataSourceFactory("SkyNews LTE-B Exoplayer", null);
		defaultDataSourceFactory = new DefaultDataSourceFactory(context, null, httpDataSourceFactory);

		chunkFactory = new DefaultDashChunkSource.Factory(defaultDataSourceFactory);
		mediaSource = new DashMediaSource(uri, defaultDataSourceFactory, chunkFactory, mainHandler, eventLogger);
	}

	public void initPlayer(Context context) {
		TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(new DefaultBandwidthMeter());
		TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
		LoadControl loadControl = new DefaultLoadControl();
		player = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
		player.setPlayWhenReady(true);
	}

	public MediaSource getMediaSource() {
		return mediaSource;
	}

	public SimpleExoPlayer getPlayer() {
		return player;
	}


}

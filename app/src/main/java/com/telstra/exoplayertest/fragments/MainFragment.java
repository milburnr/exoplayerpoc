package com.telstra.exoplayertest.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.telstra.exoplayertest.R;

/**
 * Created by russellmilburn on 19/1/17.
 */

public class MainFragment extends Fragment {

	public static final String TAG = MainFragment.class.toString();
	private String streamUri = "http://irtdashreference-i.akamaihd.net/dash/live/901161/bfs/manifestARD.mpd";
	private SimpleExoPlayerView simpleExoPlayerView;
	private SimpleExoPlayer player;
	private RetainedMediaSource data;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_fragment, container, false);
		simpleExoPlayerView = (SimpleExoPlayerView) view.findViewById(R.id.simplePlayerView);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (this.getData() == null)
		{
			this.setData(new RetainedMediaSource(getContext(), Uri.parse(streamUri)));
			this.getData().initPlayer(getContext());
		}
		player = getData().getPlayer();
		simpleExoPlayerView.setPlayer(player);
		simpleExoPlayerView.setUseController(false);

		player.prepare(getData().getMediaSource());
	}

	public void setPlayWhenReady(boolean value)
	{
		if (player != null) {
			player.setPlayWhenReady(value);
		}
	}

	public void onDestroyBySystem() {
		Log.i(TAG, "onDestroyBySystem");
	}

	public void onDestroyByUser() {
		Log.i(TAG, "onDestroyByUser");
		releasePlayer();
	}


	public void releasePlayer()
	{
		if (player != null) {
			player.release();
			player = null;
		}
		data = null;
	}

	public RetainedMediaSource getData() {
		return data;
	}

	public void setData(RetainedMediaSource data) {
		this.data = data;
	}

}

package com.telstra.exoplayertest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;

import static android.R.attr.data;
import static android.R.attr.label;

/**
 * Created by russellmilburn on 19/1/17.
 */

public class RetainedFragment<T> extends Fragment {

	public T data;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	public static <T> RetainedFragment<T> findOrCreate(FragmentManager fm, String tag)
	{
		RetainedFragment<T> retainedFragment = (RetainedFragment<T>) fm.findFragmentByTag(tag);

		if (retainedFragment == null)
		{
			retainedFragment = new RetainedFragment();
			fm.beginTransaction()
					.add(retainedFragment, tag)
					.commitAllowingStateLoss();
		}

		return  retainedFragment;
	}


	public void remove(FragmentManager fm)
	{
		if (!fm.isDestroyed()){
			fm.beginTransaction()
					.remove(this)
					.commitAllowingStateLoss();
			data = null;
		}
	}
}
